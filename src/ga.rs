use std::marker::PhantomData;
use conv::ValueFrom;
use crate::operators::{InitFunc, FitnessFunc, ParentSelection, Recombination, Mutation, SurvivalSelection, TerminationCondition};
use crate::data::{Representation, Gene, Context, Fitness};
use scoped_threadpool::Pool;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::channel;

pub struct GA<R: Representation<G>, G: Gene> {
    r: PhantomData<R>,
    g: PhantomData<G>,
    pop_size: usize,
    parent_frac: f64,
    mutation_frac: f64,
    higher_fitness_good: bool,
    core_amount: u32,
}

impl<R: 'static + Representation<G>, G: 'static + Gene> GA<R, G> {
    pub fn run_ga<F, C: Context + 'static>(
        &mut self,
        mut init_func: impl InitFunc<R, G, C>,
        fitness_func: impl FitnessFunc<R, G, F, C> + 'static,
        mut mating_select: impl ParentSelection<R, G, F, C>,
        recombination_func: impl Recombination<R, G, C> + 'static,
        mutation_func: impl Mutation<R, G, C> + 'static,
        mut survival_func: impl SurvivalSelection<R, G, F, C>,
        mut term_cond: impl TerminationCondition<R, G, F, C>,
        ctx: C)
        -> Vec<(R, F)>
        where F: Fitness + 'static,
    {
        //Get the amount of parents that is desired
        //TODO need to fix so that we can verify that it's the correct amount of parents
        let temp = f64::value_from(self.pop_size).expect("Could not turn pop_size into a f64");
        let parent_amount = temp * self.parent_frac;
        let parent_amount = parent_amount.round() as usize;

        let mut pool = Pool::new(self.core_amount);
        let ctx = Arc::new(Mutex::new(ctx));
        let recombination_func = Arc::new(recombination_func);
        let fitness_func = Arc::new(fitness_func);
        let mutation_func = Arc::new(mutation_func);

        let mut pop = Vec::with_capacity(self.pop_size);
        {
            //Initialize
            let init_pop = init_func.init(self.pop_size, &mut (ctx.lock().expect("Poisioned lock on init")));

            //Get fitness for initial population
            let (tx, rx) = channel();
            let l = init_pop.len();
            for ind in init_pop {
                let c = ctx.clone();
                let fc = fitness_func.clone();
                let tx = tx.clone();
                pool.scoped(|scope| {
                    scope.execute(move|| {
                        let fitness = fc.fitness(&ind, c);
                        tx.send((ind, fitness)).expect("Could not send inital pop fitness from thread");
                    });
                });
            }
            for _ in 0..l {
                pop.push(rx.recv().unwrap());
            }
            if self.higher_fitness_good {
                pop.sort_by(|(_, a), (_, b)| b.cmp(a));
            } else {
                pop.sort_by(|(_, a), (_, b)| a.cmp(b));
            }
        }

        loop {
            //Decide parents
            //NOTE: It does not make sense to multithread the parents because lots of
            //parent-selection needs to be synchronized.
            let parents = mating_select.select(&pop, parent_amount, &mut ctx.lock().expect("Poisioned lock at parent select"));
            //Recombine
            //We scope all of the variables so we're safe from contamination in the future
            let mut children = Vec::with_capacity(parents.len());
            {
                let (tx, rx) = channel();
                let l = parents.len();
                pool.scoped(|scope| {
                    for parent_couple in parents {
                        let c = ctx.clone();
                        let recomb = recombination_func.clone();
                        let tx = tx.clone();
                            scope.execute(move|| {
                                tx.send(recomb.recombination(parent_couple, c)).expect("Could not send inital pop fitness from thread");
                            });
                    }
                });
                for _ in 0..l {
                    let (child1, child2) = rx.recv().unwrap();
                    children.push(child1);
                    children.push(child2);
                }
            }
            //Mutate
            {
                //This figures out which children should be mutated and puts those children in
                //mut_children
                let temp = f64::value_from(children.len()).expect("Could not turn children length into a f64");
                let mutation_amount = temp * self.mutation_frac;
                let mutation_amount = mutation_amount.round() as usize;
                let mut mut_children = Vec::with_capacity(mutation_amount);
                for child in &mut children {
                    mut_children.push(child);
                }

                pool.scoped(|scope| {
                    for child in mut_children {
                        let c = ctx.clone();
                        let mf = mutation_func.clone();
                        scope.execute(move|| {
                            mf.mutate(child, c);
                        });
                    }
                });
            }
            //Evaluate child fitness
            let mut child_fit = Vec::with_capacity(children.len());
            {
                let (tx, rx) = channel();
                let l = children.len();
                pool.scoped(|scope| {
                    for child in children {
                        let c = ctx.clone();
                        let fc = fitness_func.clone();
                        let tx = tx.clone();
                            scope.execute(move|| {
                                let fitness = fc.fitness(&child, c);
                                tx.send((child, fitness)).expect("Could not send child fitness from thread");
                            });
                    }
                });
                for _ in 0..l {
                    child_fit.push(rx.recv().unwrap());
                }

                if self.higher_fitness_good {
                    child_fit.sort_by(|(_, a), (_, b)| b.cmp(a));
                } else {
                    child_fit.sort_by(|(_, a), (_, b)| a.cmp(b));
                }
            }
            //Select survivors
            pop = survival_func.select(pop, child_fit, self.pop_size, &mut ctx.lock().expect("Panic at survival lock"));
            if self.higher_fitness_good {
                pop.sort_by(|(_, a), (_, b)| b.cmp(a));
            } else {
                pop.sort_by(|(_, a), (_, b)| a.cmp(b));
            }
            //Decide whether to terminate
            if term_cond.terminate(&pop, &mut ctx.lock().expect("Panic at terminate lock")) {
                break;
            }
        }
        pop
    }
}

pub struct Builder<R: Representation<G>, G: Gene> {
    r: PhantomData<R>,
    g: PhantomData<G>,
    parent_frac: f64,
    pop_size: usize,
    higher_fitness_good: bool,
    mut_frac: f64,
    core_amount: u32,
}

impl<R: Representation<G>, G: Gene> Builder<R, G> {
    pub fn new() -> Self {
        Builder {
            r: PhantomData{},
            g: PhantomData{},
            pop_size: 100,
            parent_frac: 0.25f64,
            higher_fitness_good: false,
            mut_frac: 0.25f64,
            core_amount: 8,
        }
    }

    pub fn number_of_cores(mut self, number_of_cores: u32) -> Self {
        self.core_amount = number_of_cores;
        self
    }

    pub fn parent_fraction(mut self, parent_frac: f64) -> Self {
        self.parent_frac = parent_frac;
        self
    }

    pub fn higher_fitness_good(mut self, higher_fitness_good: bool) -> Self {
        self.higher_fitness_good = higher_fitness_good;
        self
    }

    pub fn pop_size(mut self, pop_size: usize) -> Self {
        self.pop_size = pop_size;
        self
    }

    pub fn mutation_frac(mut self, mut_frac: f64) -> Self {
        self.mut_frac = mut_frac;
        self
    }

    pub fn build(self) -> GA<R, G> {
        GA {
            r: PhantomData{},
            g: PhantomData{},
            parent_frac: self.parent_frac,
            pop_size: self.pop_size,
            higher_fitness_good: self.higher_fitness_good,
            mutation_frac: self.mut_frac,
            core_amount: self.core_amount,
        }
    }
}
