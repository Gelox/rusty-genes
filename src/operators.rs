use crate::data::{Representation, Gene, Fitness, Context};
use std::sync::{Arc, Mutex};

/// Initialization operator
pub trait InitFunc<R: Representation<G>, G: Gene, C: Context> {
    /// In: The amount of representations to create
    /// Out: A vector containing `pop_size` representations
    fn init(&mut self, pop_size: usize, ctx: &mut C) -> Vec<R>;
}

/// Multithreaded fitness function determining how close a solution is to solving the problem
pub trait FitnessFunc<R: Representation<G>, G: Gene, F: Fitness, C: Context>: Send + Sync {
    /// In: A reference to a representation
    /// Out: The fitness of the representation
    fn fitness(&self, ind: &R, ctx: Arc<Mutex<C>>) -> F;
}

/// A parent selection algorithm
pub trait ParentSelection<R: Representation<G>, G: Gene, F: Fitness, C: Context> {
    /// In: A reference to a vector containing the sorted population from best
    /// to worst with each individuals respective fitness value, the total amount of parents that ought
    /// to be produced and a context to the algorithm
    /// Out: A vector with touples containing references to the parents that ought to be mated. This
    /// will likely be updated in the future to allow for a general amount of parents rather than just
    /// two
    fn select<'a>(&mut self, pop: &'a Vec<(R, F)>, parent_amount: usize, ctx: &mut C) -> Vec<(&'a R, &'a R)>;
}

/// Multithreaded recombination operator that creates children by combining two parents
pub trait Recombination<R: Representation<G>, G: Gene, C: Context>: Send + Sync {
    /// In: A tuple of references to the parents
    /// Out: A tuple of the produced children
    fn recombination(&self, parents: (&R, &R), ctx: Arc<Mutex<C>>) -> (R, R);
}

/// Multithreaded mutation operator that does in-line modifications
pub trait Mutation<R: Representation<G>, G: Gene, C: Context>: Send + Sync {
    /// In: A reference to the representation that should
    /// be modified
    fn mutate(&self, child: &mut R, ctx: Arc<Mutex<C>>);
}

/// Operator that decides which representations get to survive to the next generation
pub trait SurvivalSelection<R: Representation<G>, G: Gene, F: Fitness, C: Context> {
    /// In: A vector with the parents and their corresponding fitness, a vector with the children
    /// and their corresponding fitness and the `survival_amount` which is the total amount of
    /// individuals that should survive
    /// Out: A vector with the representations that survived and their corresponding fitness
    fn select(&mut self, parents: Vec<(R, F)>, children: Vec<(R, F)>, survival_amount: usize, ctx: &mut C) -> Vec<(R, F)>;
}

/// Operator that decides when the GA should end
pub trait TerminationCondition<R: Representation<G>, G: Gene, F: Fitness, C: Context> {
    /// In: A reference to the population sorted from best to worst
    /// Out: True if GA should end, else false
    fn terminate(&mut self, pop: &Vec<(R, F)>, ctx: &mut C) -> bool;
}
