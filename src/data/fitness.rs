/// Trait for the fitness of a representation.
pub trait Fitness: num::Num + Eq + PartialEq + Ord + PartialOrd + Copy + std::fmt::Debug + Send + Sync {
}

impl<T: num::Num + Eq + PartialEq + Ord + PartialOrd + Copy + std::fmt::Debug + Send + Sync> Fitness for T {
}
