/// Trait for the context of the genetic algorithm. The context is used to store algorithm specific
/// data and give access to that within operators.
pub trait Context: Send + Sync {
    type Rng: rand::Rng;
    fn get_rng(&mut self) -> Self::Rng;
}
