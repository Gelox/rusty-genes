use super::gene::Gene;
/// A representation contains a chromosome which is a datastructure containing individual genes
pub trait Representation<G: Gene>: Clone + std::fmt::Debug + Send + Sync {
    fn new(chromosome: Vec<G>) -> Self;
    fn get_mut_chromosome(&mut self) -> &mut Vec<G>;
    fn get_chromosome(&self) -> &Vec<G>;
}

/// A representation where the order of the genes is an important aspect
pub trait Permutation<G: Gene>: Representation<G> {
}

/// A representation that contains a fixed amount of genes
pub trait Fixed<G: Gene>: Representation<G> {
}

