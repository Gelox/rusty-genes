/// Trait for a gene which is contained in a representation
pub trait Gene: Clone + Eq + PartialEq + std::fmt::Debug + Send + Sync {
}

impl<T: Eq + PartialEq + Clone + std::fmt::Debug + Send + Sync> Gene for T {
}

/// A bit gene that can be used for bit representations
pub trait Bit: Gene {
    fn flip(&mut self);
}

impl Bit for bool {
    fn flip(&mut self) {
        if *self == false {
            *self = true;
        } else {
            *self = false;
        }
    }
}
