use crate::data::{Representation, Gene, Fitness, Context};
use crate::operators::SurvivalSelection;
use std::marker::PhantomData;

/// The best individual from either the parents or the children survive
pub struct BestSurvives {
    _pd: PhantomData<i32>,
}

impl BestSurvives {
    pub fn new() -> Self {
        Self {
            _pd: PhantomData{}
        }
    }
}

impl<R: Representation<G>, G: Gene, F: Fitness, C: Context> SurvivalSelection<R, G, F, C> for BestSurvives {
    fn select(&mut self, parents: Vec<(R, F)>, children: Vec<(R, F)>, surival_amount: usize, _ctx: &mut C) -> Vec<(R, F)> {
        let mut v = vec![];
        let mut pp = 0;
        let mut cp = 0;
        for _ in 0..surival_amount {
            if cp < children.len() && parents[pp].1 > children[cp].1 {
                v.push(children.get(cp).unwrap().clone());
                cp += 1;
            } else {
                v.push(parents.get(pp).unwrap().clone());
                pp += 1;
            }
        }
        v
    }
}
