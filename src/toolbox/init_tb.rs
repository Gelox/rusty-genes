use crate::data::{Representation, Gene, Permutation, Context};
use crate::operators::InitFunc;
use rand::seq::SliceRandom;

/// Generate a random permutation of gene `G` the representation is generated from the provided
/// vector
pub struct RandomPermutationInit<G: Gene> {
    from: Vec<G>,
}

impl<G: Gene> RandomPermutationInit<G> {
    pub fn new(from: Vec<G>) -> Self {
        RandomPermutationInit { from }
    }
}

impl<R: Permutation<G>, G: Gene, C: Context> InitFunc<R, G, C> for RandomPermutationInit<G> {
    fn init(&mut self, pop_size: usize, _ctx: &mut C) -> Vec<R> {
        let mut pop = Vec::with_capacity(pop_size);
        let mut rng = rand::thread_rng();
        for _ in 0..pop_size {
            let mut from = self.from.clone();
            from.shuffle(&mut rng);
            let permutation = Representation::new(from);
            pop.push(permutation);
        }
        pop
    }
}

