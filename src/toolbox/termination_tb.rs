use crate::data::{Representation, Fitness, Gene, Context};
use crate::operators::TerminationCondition;

/// Stop after given number of generations
pub struct StopAfterGenerations {
    amount_of_generations: usize,
    counter: usize,
}

impl StopAfterGenerations {
    pub fn new(amount_of_generations: usize) -> Self {
        StopAfterGenerations { amount_of_generations, counter: 0 }
    }
}

impl<R: Representation<G>, G: Gene, F: Fitness, C: Context> TerminationCondition<R, G, F, C> for StopAfterGenerations {
    fn terminate(&mut self, _pop: &Vec<(R, F)>, _ctx: &mut C) -> bool {
        self.counter += 1;
        if self.counter < self.amount_of_generations {
            return false;
        } else {
            return true;
        }
    }
}
