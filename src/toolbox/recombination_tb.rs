use crate::data::{Permutation, Fixed, Gene, Context};
use crate::operators::Recombination;
use rand::Rng;
use rand::seq::SliceRandom;
use std::marker::PhantomData;

/// Combines two permutations by picking a random crossover point and taking the genes from the
/// right side of the point from each parent to two different children. Then puts in the remaining
/// genes in the order they appear in the other parent.
pub struct PermutationSinglePointCrossover {
    _pd: PhantomData<i32>,
}

impl PermutationSinglePointCrossover {
    pub fn new() -> Self {
        Self {
            _pd: PhantomData{}
        }
    }
}

use std::sync::{Arc, Mutex};
impl<R: Permutation<G>, G: Gene, C: Context> Recombination<R, G, C> for PermutationSinglePointCrossover {
    fn recombination(&self, parents: (&R, &R), ctx: Arc<Mutex<C>>) -> (R, R) {
        let (parent1, parent2) = parents;
        let chr1 = parent1.get_chromosome();
        let chr2 = parent2.get_chromosome();
        let r;
        {
            let mut rng = ctx.lock().unwrap().get_rng();
            r = rng.gen::<usize>() % chr1.len();
        }
        let mut child1: Vec<G> = Vec::with_capacity(chr1.len());
        let mut child2: Vec<G> = Vec::with_capacity(chr1.len());
        child1.extend_from_slice(&chr1[..r]);
        child2.extend_from_slice(&chr2[..r]);
        for gene in parent2.get_chromosome().iter() {
            if !child1.contains(&gene) {
                child1.push(gene.clone());
            }
        }
        for gene in parent1.get_chromosome().iter() {
            if !child2.contains(&gene) {
                child2.push(gene.clone());
            }
        }
        let child1 = R::new(child1);
        let child2 = R::new(child2);
        (child1, child2)
            //v.push(child1);
            //v.push(child2);
    }
}

/// Creates N crosspoints and alternates between the parents for each of those crosspoints to
/// producing two children with inverted alterations.
pub struct NPointCrossover {
    n_points: usize,
}

impl NPointCrossover {
    pub fn new(n_points: usize) -> Self {
        NPointCrossover {
            n_points
        }
    }
}

impl<R: Fixed<G>, G: Gene, C: Context> Recombination<R, G, C> for NPointCrossover {
    fn recombination(&self, (parent1, parent2): (&R, &R), ctx: Arc<Mutex<C>>) -> (R, R) {
        let chr1 = parent1.get_chromosome();
        let chr2 = parent2.get_chromosome();

        //Get the n points
        let mut n_array = Vec::with_capacity(chr1.len());
        for i in 0..chr1.len() {
            n_array.push(i);
        }

        {
            let mut rng = ctx.lock().unwrap().get_rng();
            n_array.shuffle(&mut rng);
        }

        let mut cross_points = Vec::with_capacity(self.n_points);
        for i in 0..self.n_points {
            match n_array.get(i) {
                Some(point) => {
                    cross_points.push(point);
                }
                None => break,
            }
        }
        cross_points.sort();

        //Create children
        let mut child1: Vec<G> = Vec::with_capacity(chr1.len());
        let mut child2: Vec<G> = Vec::with_capacity(chr1.len());

        //Alternate between parents for slice
        let mut last = 0;
        let mut inverse = false;
        for point in cross_points {
            if inverse {
                child1.extend_from_slice(&chr2[last..*point]);
                child2.extend_from_slice(&chr1[last..*point]);
                inverse = false;
            } else {
                child1.extend_from_slice(&chr1[last..*point]);
                child2.extend_from_slice(&chr2[last..*point]);
                inverse = true;
            }
            last = *point;
        }
        //Put last slice in
        if inverse {
            child1.extend_from_slice(&chr2[last..]);
            child2.extend_from_slice(&chr1[last..]);
        } else {
            child1.extend_from_slice(&chr1[last..]);
            child2.extend_from_slice(&chr2[last..]);
        }

        let child1 = R::new(child1);
        let child2 = R::new(child2);
        (child1, child2)
    }
}

/// Randomly puts the genes of a parent into one child and the genes of the other into the other
/// child
pub struct UniformCrossover {
    _pd: PhantomData<i32>,
}

impl UniformCrossover {
    pub fn new() -> Self {
        Self {
            _pd: PhantomData{}
        }
    }
}

impl<R: Fixed<G>, G: Gene, C: Context> Recombination<R, G, C> for UniformCrossover {
    fn recombination(&self, (parent1, parent2): (&R, &R), ctx: Arc<Mutex<C>>) -> (R, R) {
        let chr1 = parent1.get_chromosome();
        let chr2 = parent2.get_chromosome();

        let mut bools = Vec::with_capacity(chr1.len());
        {
            let mut rng = ctx.lock().unwrap().get_rng();
            for _ in 0..chr1.len() {
                bools.push(rng.gen::<bool>())
            }
        }

        let mut child1: Vec<G> = Vec::with_capacity(chr1.len());
        let mut child2: Vec<G> = Vec::with_capacity(chr1.len());
        for i in 0..chr1.len() {
            if bools.pop().unwrap() {
                child1.push(chr1[i].clone());
                child2.push(chr2[i].clone());
            } else {
                child1.push(chr2[i].clone());
                child2.push(chr1[i].clone());
            }
        }
        let child1 = R::new(child1);
        let child2 = R::new(child2);
        (child1, child2)
    }
}
