use crate::data::{Representation, Permutation, Gene, Bit, Context};
use crate::operators::Mutation;
use rand::Rng;
use std::sync::{Arc, Mutex};

/// Mutation for permutations that swap two elements around randomly
pub struct SwapMutation {
    amount_of_swaps: usize,
}

impl SwapMutation {
    pub fn new(amount_of_swaps: usize) -> Self {
        SwapMutation { amount_of_swaps }
    }
}

impl<R: Permutation<G>, G: Gene, C: Context> Mutation<R, G, C> for SwapMutation {
    fn mutate(&self, child: &mut R, ctx: Arc<Mutex<C>>) {
        let chromo = child.get_mut_chromosome();
        let l = chromo.len();
        let mut r = vec![];
        {
            let mut rng = ctx.lock().expect("Could not aquire lock in SwapMutation").get_rng();
            for _ in 0..self.amount_of_swaps {
                r.push(rng.gen::<usize>() % l);
                r.push(rng.gen::<usize>() % l);
            }
        }
        for _ in 0..self.amount_of_swaps {
            let r1 = r.pop().unwrap();
            let r2 = r.pop().unwrap();
            let temp = chromo[r1].clone();
            chromo[r1] = chromo[r2].clone();
            chromo[r2] = temp;
        }
    }
}

/// Mutation that flips bit genes randomly
pub struct BitFlipMutation {
    amount_of_flips: usize,
}

impl BitFlipMutation {
    pub fn new(amount_of_flips: usize) -> Self {
        BitFlipMutation { amount_of_flips }
    }
}

impl<R: Representation<G>, G: Bit, C: Context> Mutation<R, G, C> for BitFlipMutation {
    fn mutate(&self, child: &mut R, ctx: Arc<Mutex<C>>) {
        let chromo = child.get_mut_chromosome();
        let l = chromo.len();
        {
            let mut rng = ctx.lock().unwrap().get_rng();
            for _ in 0..self.amount_of_flips {
                let r = rng.gen::<usize>() % l;
                chromo[r].flip();
            }
        }
    }
}

/// Mutation that adds a number randomly to i32 genes. The added number is between the bounds.
pub struct ArithmeticMutation {
    amount_of_operations: usize,
    bounds: (i32, i32),
}

impl ArithmeticMutation {
    pub fn new(amount_of_operations: usize, bounds: (i32, i32)) -> Self {
        ArithmeticMutation {amount_of_operations, bounds}
    }
}

impl<R: Representation<i32>, C: Context> Mutation<R, i32, C> for ArithmeticMutation {
    fn mutate(&self, child: &mut R, ctx: Arc<Mutex<C>>) {
        let chromo = child.get_mut_chromosome();
        let l = chromo.len();
        {
            let mut rng = ctx.lock().unwrap().get_rng();
            for _ in 0..self.amount_of_operations {
                let r = rng.gen::<usize>() % l;
                let v = rng.gen::<i32>() % (self.bounds.1 - self.bounds.0);
                let value = v + self.bounds.0;
                chromo[r] += value;
            }
        }
    }
}

pub struct NoMutation;

impl NoMutation {
    pub fn new() -> Self {
        Self {}
    }
}

impl<R: Representation<G>, G: Gene, C: Context> Mutation<R, G, C> for NoMutation {
    fn mutate(&self, _child: &mut R, _ctx: Arc<Mutex<C>>) {
    }
}

