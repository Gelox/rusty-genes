use crate::data::Context;

/// BasicContext contains a RNG
pub struct BasicContext {
}

impl BasicContext {
    pub fn new() -> Self {
        BasicContext { }
    }
}

impl Context for BasicContext {
    type Rng = rand::rngs::ThreadRng;
    fn get_rng(&mut self) -> Self::Rng {
        rand::thread_rng()
    }
}
