use crate::data::{Representation, Permutation, Gene, Fixed};

/// Basic representation for any gene
#[derive(Clone, Debug)]
pub struct BasicRepresentation<G> {
    chromosome: Vec<G>,
}

impl<G: Gene> Representation<G> for BasicRepresentation<G> {
    fn new(chromosome: Vec<G>) -> Self {
        BasicRepresentation { chromosome }
    }
    fn get_mut_chromosome(&mut self) -> &mut Vec<G> {
        &mut self.chromosome
    }
    fn get_chromosome(&self) -> &Vec<G> {
        &self.chromosome
    }
}

/// Permutation representation if the order matters
#[derive(Clone, Debug)]
pub struct PermutationRepresentation<G> {
    chromosome: Vec<G>,
}

impl<G: Gene> Permutation<G> for PermutationRepresentation<G> {
}

impl<G: Gene> Representation<G> for PermutationRepresentation<G> {
    fn new(chromosome: Vec<G>) -> Self {
        PermutationRepresentation { chromosome }
    }
    fn get_mut_chromosome(&mut self) -> &mut Vec<G> {
        &mut self.chromosome
    }
    fn get_chromosome(&self) -> &Vec<G> {
        &self.chromosome
    }
}

/// Representation that has to be of a fixed size where each instance has the same size
#[derive(Debug, Clone)]
pub struct FixedVector<G> {
    chromosome: Vec<G>,
}

impl<G: Gene> Representation<G> for FixedVector<G> {
    fn new(chromosome: Vec<G>) -> Self {
        FixedVector { chromosome }
    }
    fn get_mut_chromosome(&mut self) -> &mut Vec<G> {
        &mut self.chromosome
    }
    fn get_chromosome(&self) -> &Vec<G> {
        &self.chromosome
    }
}

impl <G: Gene> Fixed<G> for FixedVector<G> {
}

/// Combination of both the fixed and permutation representations
#[derive(Debug, Clone)]
pub struct FixedPermutation<G> {
    chromosome: Vec<G>,
}

impl<G: Gene> Representation<G> for FixedPermutation<G> {
    fn new(chromosome: Vec<G>) -> Self {
        FixedPermutation { chromosome }
    }
    fn get_mut_chromosome(&mut self) -> &mut Vec<G> {
        &mut self.chromosome
    }
    fn get_chromosome(&self) -> &Vec<G> {
        &self.chromosome
    }
}

impl<G: Gene> Permutation<G> for FixedPermutation<G> {
}

impl <G: Gene> Fixed<G> for FixedPermutation<G> {
}
