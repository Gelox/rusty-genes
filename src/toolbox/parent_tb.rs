use crate::data::{Representation, Gene, Fitness, Context};
use crate::operators::ParentSelection;
use rand::Rng;
use std::marker::PhantomData;

/// Select the most fit parents
pub struct BestParentSelection {
    _pd: PhantomData<i32>,
}

impl BestParentSelection {
    pub fn new() -> Self {
        Self {
            _pd: PhantomData {}
        }
    }
}

impl<R, G, F, C> ParentSelection<R, G, F, C> for BestParentSelection
    where 
    R: Representation<G>,
    G: Gene,
    F: Fitness,
    C: Context
{
    fn select<'a>(&mut self, pop: &'a Vec<(R, F)>, parent_amount: usize, _ctx: &mut C) -> Vec<(&'a R, &'a R)> {
        let mut v = vec![];
        for i in 0..parent_amount / 2 {
            v.push((&pop[i*2].0, &pop[i*2 + 1].0));
        }
        v
    }
}

/// Selects parents randomly where a representation has a chance to be picked proportional to its
/// fitness.
pub struct RouletteWheelSelection {
    _pd: PhantomData<i32>,
}

impl RouletteWheelSelection {
    pub fn new() -> Self {
        Self {
            _pd: PhantomData {}
        }
    }
}

//TODO this is currently only implemented for Fitness = usize this is because we need to constrain
//it in such a way that rng can produce random numbers for it. Hopefully a workaround can be found
//in the future.
impl<R, G, C> ParentSelection<R, G, usize, C> for RouletteWheelSelection
    where 
    R: Representation<G>,
    G: Gene,
    C: Context,
{
    fn select<'a>(&mut self, pop: &'a Vec<(R, usize)>, parent_amount: usize, ctx: &mut C) -> Vec<(&'a R, &'a R)> {
        let mut parents = Vec::with_capacity(parent_amount);
        let mut fit_sum = 0;
        for (_, f) in pop {
            fit_sum += *f;
        }
        let mut rng = ctx.get_rng();
        for _ in 0..parent_amount {
            let r = rng.gen::<usize>() % fit_sum;
            let mut s = 0usize;
            for (ind, fit) in pop {
                s += fit;
                if r <= s {
                    parents.push(ind);
                    break;
                }
            }
        }
        let mut coupled_parents = Vec::with_capacity(parent_amount / 2);
        for i in 0..parent_amount / 2 {
            coupled_parents.push((parents[i*2], parents[i*2 + 1]));
        }
        coupled_parents
    }
}

/// Works like RouletteWheelSelection but instead of a chance proportional to fitness ranks
/// individuals on their fitness and then picks proportional to rank
pub struct RankSelection {
    _pd: PhantomData<i32>,
}

impl RankSelection {
    pub fn new() -> Self {
        Self {
            _pd: PhantomData {}
        }
    }
}

impl<R, G, F, C> ParentSelection<R, G, F, C> for RankSelection
    where 
    R: Representation<G>,
    G: Gene,
    F: Fitness,
    C: Context,
{
    fn select<'a>(&mut self, pop: &'a Vec<(R, F)>, parent_amount: usize, ctx: &mut C) -> Vec<(&'a R, &'a R)> {
        let mut parents = Vec::with_capacity(parent_amount);
        let mut fit_sum = 0;
        for i in 0..pop.len() {
            fit_sum += i;
        }
        let mut rng = ctx.get_rng();
        for _ in 0..parent_amount {
            let r = rng.gen::<usize>() % fit_sum;
            let mut s = 0usize;
            for (rank, (ind, _)) in pop.iter().rev().enumerate() {
                s += rank;
                if r <= s {
                    parents.push(ind);
                    break;
                }
            }
        }
        let mut coupled_parents = Vec::with_capacity(parent_amount / 2);
        for i in 0..parent_amount / 2 {
            coupled_parents.push((parents[i*2], parents[i*2 + 1]));
        }
        coupled_parents
    }
}

