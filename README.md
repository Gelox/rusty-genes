# Rusty-genes

Rusty-genes is a genetic algorithm framework that attempts to leverage Rusts 
fantastic type system in order to provide a dynamic and extensible toolbox of 
general GA  operators and a multithreaded environment to run these in while also detecting 
semantical inconsistencies at compile time in order to prevent bugs.

## What is a genetic algorithm?
A genetic algorithm is a type of meta-heuristic that attempts to find solutions
to problems (often complex combinatorial ones) by a process similar to biological
evolution.

The algorithm functions like the following:
Each possible solution to the problem is represented as a set of genes, called a
chromosome or an individual.
1. Initialize a population of individuals
2. Evaluate the fitness of each individual
3. Select which individuals should reproduce based (usually) on their fitness
4. Mate the chosen individuals and produce children
5. Mutate some of the children
6. Evaluate the fitness of the children
7. Choose which of the children and which of the original population survives
8. Either terminate if termination condition is met or repeat from step 3

## Goals
One of the benefits of GAs is the generalizability of the meta-heuristic. It is
very possible to solve very different problems with only very minor changes to
the algorithm. This generalizability can be seen very clearly in the fact that
many very different problems use the same GA representations and the same GA
operators. Sometimes with slight differences between each other.

Most of the rust GA frameworks that I found do not provide features that allow for
this generalizability. They usually mostly provide a simulator to run the algorithm
in but no way for the user to choose already written operators that fit general
problems.
Instead they force the user to implement a representation, a fitness function, a
recombination function and a mutation function.
Usually they don't allow the user to decide or design their own parent or surival
selection algorithm even if this is sought after.

The 6 goals for this crate are
* Give users the possibility to reuse general representations and operators.

* Provide a simple API for users to create their own representation and operators.

* Utilize the rust type system to catch semantic bugs at compile-time

* Allow for user implementation of only certain operators but reuse of others.

* Allow for high customizability with read-only memory for custom operators and
read/write memory to communicate between operators via a GA context.

* Provide a high-performant simulator with multi-threading.


### Operator API
A user can implement the recombination, mutation, parent selection, survival selection,
termination condition operators if they so choose.

A fitness function has to be implemented as this is problem specific and can not
be generalized.

Operators can enforce semantic correctness by utilizing the rust type system and
can catch semantic bugs at compiletime. For example if a permutation representation
is used together with an operation that does not gurantee to only reorder the
representation then a semantic bug has appeared. This can be caught by making sure
that representations that are permutations implement the Permutation trait
and operators that are compatible with these representations are implemented for
permutations.

Each operator has access to the GA context, which is a trait that can be implemented
by a user that wants to pass very specific data between operators.
Each operator also has access to their own read-only data that can be used to store
operator specific constants.

A toolbox of common operators and representations are provided so that a user can
combine this in whichever way they want.

### Multithreading
The recombination, mutation and fitness operators are multi-threaded in order to allow
increase in performance.
The remaining operators have been left unthreaded since these algorithms usually require
synchronization.

### Future
I hope to at some point remove the need to have exactly two parents and generalize this.
However since most basic operators use exactly two parents I've left it as is for now.

Currently not very many options are given for how the simulator runs, I hope to 
implement more options like this.

The current implementation is not very easy to debug, I hope to create a better
logging system in the future.

### Similar crates
[oxigen](https://github.com/Martin1887/oxigen)

[RsGenetic](https://github.com/m-decoster/RsGenetic)

[darwin-rs](https://github.com/willi-kappler/darwin-rs)

[Differential Evolution](https://github.com/martinus/differential-evolution-rs)

[evolve-sbrain](https://github.com/NoraCodes/evolve-sbrain)

[Nodevo](https://github.com/bgalvao/nodevo)

[revonet](https://github.com/yurytsoy/revonet)

[evo-rs](https://github.com/mneumann/evo-rs)

### Rust
[Here is a basic resource on how to install the rust compiler](https://www.rust-lang.org/tools/install)

[Here is the rust book for anyone that is unfamiliar and wants to get started](https://doc.rust-lang.org/book/)