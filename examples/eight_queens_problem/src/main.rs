use std::convert::TryFrom;
use rusty_genes::operators::*;
use rusty_genes::data::context::*;
use rusty_genes::data::representation::*;
use rusty_genes::toolbox::init_tb::*;
use rusty_genes::toolbox::mutation_tb::*;
use rusty_genes::toolbox::parent_tb::*;
use rusty_genes::toolbox::recombination_tb::*;
use rusty_genes::toolbox::survival_tb::*;
use rusty_genes::toolbox::representation_tb::*;
use rusty_genes::toolbox::context_tb::*;
use rusty_genes::toolbox::termination_tb::*;
use rusty_genes::ga::*;
use std::sync::{Arc, Mutex};

fn main() {
    let init_func = RandomPermutationInit::new(vec![0i32, 1, 2, 3, 4, 5, 6, 7]);
    let fit_func = EightQueenFit {};
    let recomb_func = PermutationSinglePointCrossover::new();
    let mating_func = BestParentSelection::new();
    let survival_func = BestSurvives::new();
    let mut_func = SwapMutation::new(2);
    let term_cond = StopAfterGenerations::new(50);
    let ctx = BasicContext::new();
    let builder: Builder<PermutationRepresentation<i32>, i32> = Builder::new()
        .pop_size(100)
        .parent_fraction(0.5)
        .higher_fitness_good(false)
        .number_of_cores(8)
        .mutation_frac(0.5);
    let mut ga = builder.build();
    use std::time::Instant;
    let before = Instant::now();
    let pop = ga.run_ga(init_func, fit_func, mating_func, recomb_func, mut_func, survival_func, term_cond, ctx);
    println!("Run time: {:?}", before.elapsed());
    println!("{:?}", pop[0]);
}

struct EightQueenFit;
impl<R: Representation<i32>, C: Context> FitnessFunc<R, i32, usize, C> for EightQueenFit {
    fn fitness(&self, ind: &R, _ctx: Arc<Mutex<C>>) -> usize {
        let mut fitness = 0;
        for i in 0i32..8 {
            for j in 0i32..8 {
                if i != j && (i - j).abs() == 
                    (ind.get_chromosome()[usize::try_from(i).unwrap()] - ind.get_chromosome()[usize::try_from(j).unwrap()]).abs() {
                    fitness += 1;
                }
            }
        }
        fitness
    }
}
